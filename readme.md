# ![logo de genipy](genipy-64x64.png) Génipy

Un version française de bibliothèque zenity pour python !

Comme PyZenity ne semble plus maintenu, et que j'ai besoin d'une bibliothèque à jour, là voilà !

## Installation

Assurez-vous que zenity est installé sur votre système :

```
apt update
apt install zenity
```

Installez genipy

```
pip install git+https://gitlab.com/Breizhux/genipy.git
```

## Run in python code :

```
import genipy

#simple usage :
question = genipy.GenipyQuestion()
print(question.exec()) #True if ok else False

#to get directly the result :
result = genipy.GenipyQuestion().exec()


#advanced usage :
def my_callback(*args) :
    print("is my callback")

progress_bar = genipy.GenipyProgressBar("no-cancel", "auto-close", "pulsate", callback=my_callback, width=400, text="Publication en cours")
progress_bar.start()
progress_bar.text("Set the text")
progress_bar.progress(50) #set progress bar to 50%
progress_bar.close()      #close progress and wait end of process

#available operations :
obj.read()        #get standard output
obj.error()       #get standard error
obj.write("text") #write text
obj.get_outputs() #get tuple of (returncode, output, error)
obj.get_output()  #get the utile output
```

**Note :**

* La fonction callback n'est appelée que si la fonction est appellée dans son thread ou avec l'option `wait=True` passé à exec().
* La fonction `get_output()` renvoit par défaut la sortie standard. Mais selon la fonction appelée, le retour peut varier. Par exemple, on attend aucun retour texte de `zenity --error`, donc seul `returncode` est renvoyé. Idem pour `zenity --question`. En revanche `zenity --calendar` renverra la sortie standard parce qu'on attend la date sélectionnée.
* `get_outputs()` retourne le tuple suivant : (return code, output, error).
* La fonction callback est appelée avec les arguments retournés par `get_outputs()`. N'oubliez donc pas de spécifier \*args ;)

**Fonctionnalités disponibles** :

* --question : `GenipyQuestion()`
* --error : `GenipyError()`
* --progress : `GenipyProgress()`
* --calendar : `GenipyCalendar()`
* --file-selection : `GenipyFileSelection()`
* --forms : `GenipyForms()`

    ```python
    #Exemple : il est nécessaire que les arguments de zenity --forms répétable du style --add-entry="Nom" soit passé en liste.
    GenipyForms("add-entry=\"Nom\"", "add-entry=\"Prénom\"", "add-password=\"Password\"", title="Mon titre"))
    ```
* --list : `GenipyList()`

    ```python
    #Exemple : idem que pour GenipyForms
    liste = GenipyList(
        "column=\"Fruits\"",
        "checklist",
        "column=\"Sélectionné\"",
        "TRUE Apple FALSE Banane TRUE Orange",
        title = "Fruits ?"
    )
    #Attention, petite subtilité pour les listes sans la check. Il faut mettre un espace avant la liste des éléments.
    liste = GenipyList(
        "column=\"Fruits\"",
        #↓ici, sinon la chaîne sera transformé en : --Apple --Banane --Orange
        " Apple Banane Orange",
        title = "Fruits ?")
    ```
* --entry : `GenipyEntry()`