#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

import setuptools

setuptools.setup(
    name = 'genipy',
    version = '0.1.0',
    description = 'Bidding python for zenity.',
	#license = "WTFPL",
    url = 'https://gitlab.com/breizhux/genipy',
    author = 'Breizhux',
    author_email = 'xavier.lanne@gmx.fr',
    packages = setuptools.find_packages(
        exclude = [
            'bin',
            'lib',
            '__pycache__',
            'pyvenv.cfg',
            '.gitignore',
            'requirements.txt',
        ],
    ),
    #package_data = {'mdinja' : [
    #    'styles/*',
    #]},
    #include_package_data=True,
    install_requires = [
    #    'jinja2'
    ],
	python_requires = ">=3.8",
)
