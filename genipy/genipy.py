#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

import subprocess
from threading import Thread


class Genipy(Thread) :
    def __init__(self, tool, *args, callback=None, **kwargs) :
        Thread.__init__(self)
        self.command = f"zenity --{tool} "
        #add positionnal argument
        for i in args :
            if i.startswith(" ") or i.startswith("TRUE") or i.startswith("FALSE") :
                self.command += f"{i.strip()} "
            else :
                self.command += f"--{i} "
        #add named arguments
        for i in kwargs :
            self.command += f" --{i.replace('_', '-')}"
            if isinstance(kwargs[i], str) :
                self.command += f"=\"{kwargs[i]}\""
            elif isinstance(kwargs[i], int) :
                self.command += f"={kwargs[i]}"
        self.__process = None
        self.__callback = callback
        self.__outputs = None #(exit code, output, error)

    def __iter__(self) :
        for i in self.process.stdout.readlines() :
            yield i

    def run(self) :
        """ Run command in a thread an call callback function at the end."""
        self.exec(wait=True)

    def exec(self, wait=True, callback=None) :
        """ Just start the command. If wait=True : wait the end of command and run callback
        function before return. Output is None if no wait, else is (status_code, output, error)"""
        self.__process = subprocess.Popen(self.command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE,
            shell=True,
            encoding='utf-8')
        if wait :
            self.wait()
            self.__outputs = (self.exit_code(), self.read(), self.error())
            if self.__callback is not None :
                self.__callback(*self.__outputs)
            if callback is not None :
                callback(*self.__outputs)
            return self.get_output()

    def write(self, text) :
        """ Write text in command."""
        self.__process.stdin.write(f"{text}\n")
        self.__process.stdin.flush()

    def exit_code(self) :
        """ Return the exit code of command."""
        return self.__process.returncode

    def read(self) :
        """ Read standart output."""
        return self.__process.stdout.read().strip("\n")

    def error(self) :
        """ Read error output."""
        return self.__process.stderr.read().strip("\n")

    def get_output(self) :
        """ Return stdout only."""
        return self.__outputs[1]

    def get_outputs(self) :
        """ Return tuple of : return code, stdout, stderr."""
        return self.__outputs

    def wait(self) :
        """ Wait the end of process."""
        return self.__process.wait()

    def set_callback(self, callback) :
        """ Set a new callback."""
        self.__callback = callback



class GenipyInfo(Genipy) :
    def __init__(self, *args, callback=None, **kwargs) :
        Genipy.__init__(self, "info", *args, callback=callback, **kwargs)

    def get_output(self) :
        """ Return the exit code."""
        return super().get_outputs()[0]


class GenipyError(Genipy) :
    def __init__(self, *args, **kwargs) :
        Genipy.__init__(self, "error", *args, **kwargs)

    def get_output(self) :
        """ Return the exit code."""
        return super().get_outputs()[0]


class GenipyQuestion(Genipy) :
    def __init__(self, *args, callback=None, **kwargs) :
        Genipy.__init__(self, "question", *args, callback=callback, **kwargs)

    def get_output(self) :
        """ Return the answer (True or False)."""
        return not bool(super().get_outputs()[0])


class GenipyProgress(Genipy) :
    def __init__(self, *args, **kwargs) :
        Genipy.__init__(self, "progress", *args, **kwargs)

    def text(self, text:str) :
        """ Change text of progress bar."""
        self.write(f"# {text}")

    def progress(self, n:int) :
        """ Change progression of progress bar."""
        self.write(f"{n}")

    def close(self) :
        """ Terminate and close the progress bar."""
        self.progress("100")
        self.wait()


class GenipyCalendar(Genipy) :
    def __init__(self, *args, **kwargs) :
        Genipy.__init__(self, "calendar", *args, **kwargs)

class GenipyEntry(Genipy) :
    def __init__(self, *args, **kwargs) :
        Genipy.__init__(self, "entry", *args, **kwargs)


class GenipyFileSelection(Genipy) :
    def __init__(self, *args, **kwargs) :
        if "separator" in kwargs :
            self.__separator = kwargs["separator"]
        else :
            self.__separator = "|" #default of zenity command
        Genipy.__init__(self, "file-selection", *args, **kwargs)

    def get_output(self) :
        """ Return the list of values."""
        selection = super().get_outputs()[1].split(self.__separator)
        if len(selection) == 1 :
            return selection[0]
        return selection


class GenipyForms(Genipy) :
    def __init__(self, *args, **kwargs) :
        if "separator" in kwargs :
            self.__separator = kwargs["separator"]
        else :
            self.__separator = "|" #default of zenity command
        Genipy.__init__(self, "forms", *args, **kwargs)

    def get_output(self) :
        """ Return the list of values."""
        return super().get_outputs()[1].split(self.__separator)


class GenipyList(Genipy) :
    def __init__(self, *args, **kwargs) :
        if "separator" in kwargs :
            self.__separator = kwargs["separator"]
        else :
            self.__separator = "|" #default of zenity command
        Genipy.__init__(self, "list", *args, **kwargs)

    def get_output(self) :
        """ Return the list of values."""
        return super().get_outputs()[1].split(self.__separator)



def coucou(*args) :
    print("ok callback")

if __name__ == "__main__" :
    from time import sleep
    test = "entry"

    def callback_func(*args) :
        print("From callback :", *args)

    if test == "progress_bar" :
        progress_bar = GenipyProgress("no-cancel", "auto-close", "pulsate", callback=callback_func, width=400, text="Publication en cours")
        print(progress_bar.command)
        progress_bar.start()
        sleep(1)
        progress_bar.text("coucou")
        sleep(1)
        progress_bar.progress(50)
        sleep(0.5)
        progress_bar.text("Nettoyage")
        sleep(0.7)
        progress_bar.close()

    elif test == "error" :
        error = GenipyError(width=500, title="Coucou", text="An error is occured !", icon_name="error")
        print(error.command)
        error.start()
        sleep(1)
        print("ok pendant l'exec")
        error.wait()
        print(error.get_output())

    elif test == "info" :
        info = GenipyInfo(width=250, title="Coucou", text="I have an information for you !", icon_name="info")
        print(info.command)
        info.exec()
        print(info.get_output())

    elif test == "entry" :
        entry = GenipyEntry(width=250, title="Coucou", entry_text="I have an information for you !")
        print(entry.command)
        entry.exec()
        print(entry.get_output())

    elif test == "question" :
        question = GenipyQuestion()
        print(question.command)
        print(question.exec())
        print(GenipyQuestion().exec(wait=True))

    elif test == "calendar" :
        calendar = GenipyCalendar(date_format="%Y-%m-%d")
        print(calendar.command)
        print(calendar.exec())
        print(calendar.get_output()[1])

    elif test == "forms" :
        forms = GenipyForms(
            'add-entry="Prénom"',
            'add-entry="Nom"',
            'add-entry="Courriel"',
            'add-calendar="Anniversaire"',
            title="Ajout d'un ami",
            text="Saisissez les informations concernant votre ami.",
        )
        print(forms.command)
        print(forms.exec())
        print(forms.get_output())

    elif test == "list" :
        liste = GenipyList(
            "column=\"Fruits\"",
            "checklist",
            "column=\"Sélectionné\"",
            "TRUE Apple FALSE Banane TRUE Orange",
            title = "Fruits ?")
        liste = GenipyList(
            "column=\"Fruits\"",
            " Apple Banane Orange",
            title = "Fruits ?")
        print(liste.command)
        print(liste.exec())
        print(liste.get_output())
